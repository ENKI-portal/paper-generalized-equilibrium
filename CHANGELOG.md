# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2022-06-07 (ghiorso)
### Changed
- Update Jupyter book and minor reorganization of text.

## [0.5.0] - 2020-06-05 (ghiorso)
### Added
- Initial upload; testing of CI integration and Docker/Pages deployment.
