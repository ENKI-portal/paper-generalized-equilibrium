#  Minimizing the generalized potential

Equation {eq}`eq8` is a non-linear function of T, P, and ${\bf{n}}_\phi$. At constant temperature and pressure, an equilibrium system may be found by minimizing this equation with respect to all ${\bf{n}}_{\phi}$ subject to all relevant statements of Equation {eq}`eq5` and to Equation {eq}`eq16`. This problem is one of non-linear optimization subject to non-linear constraints. There are numerous methods for computing numerical solutions to this problem. One of the computationally fastest and proven methods (Ghiorso and  Kelemen, 1987) is based on a second-order Newton iterative technique that employs explicit second-order derivatives.

We transform Equation {eq}`eq8` to its Lagrangian form

```{math}
:label: eq26
\Lambda  = G - \sum\limits_j^f {\left( {{\bf{R}}_{{\phi _j}}^T{{\bf{C}}_e}{{\bf{n}}_\phi }} \right)\left( {{\bf{R}}_{{\phi _j}}^T\mu } \right)}  - {\left[ {\begin{array}{*{20}{c}}
{{\lambda _{{n_1}}}}\\
 \vdots \\
{{\lambda _{{n_{c - f}}}}}
\end{array}} \right]^T}\left( {{\bf{V}}_{\left. f \right|f}^T{{\bf{C}}_e}{{\bf{n}}_\phi } - {\bf{V}}_{\left. f \right|f}^T{{\bf{b}}_c}} \right) - \sum\limits_j^f {{\lambda _{{\phi _j}}}\left( {{\bf{R}}_{{\phi _j}}^T\mu  - {\mu _{{\phi _j}}}} \right)} 
```

by introducing Lagrange multipliers, ${{\lambda _{{n_1}}}}$ to ${{\lambda _{{n_{c - f}}}}}$ and ${{\lambda _{{\phi _1}}}}$ to ${{\lambda _{{\phi _f}}}}$ whose values are never negative and should tend towards zero as the minimum of $\Lambda$ is achieved. The third, fourth and fifth terms of Equation {eq}`eq26` derive from the equality constraints. These equality constraints can be rearranged into a zero vector of length $c$:

```{math}
:label: eq27
\left[ {\begin{array}{*{20}{c}}
{{\bf{V}}_{\left. f \right|f}^T{{\bf{C}}_e}{{\bf{n}}_\phi} - {\bf{V}}_{\left. f \right|f}^T{{\bf{b}}_c}}\\
{\sum\limits_k^c {{c_{{\phi _1}k}}{\mu _k}}  - {\mu _{{\phi _1}}}}\\
 \vdots \\
{\sum\limits_k^c {{c_{{\phi _f}k}}{\mu _k}}  - {\mu _{{\phi _f}}}}
\end{array}} \right]
```

This vector can be differentiated with respect to the mole numbers of phases in the system to define a “linearized” constraint matrix, ${\bf{A}}$,

```{math}
:label: eq28
{\bf{A}} = \left[ {\begin{array}{*{20}{c}}
{{\bf{C}}_e^T{{\bf{V}}_{\left. f \right|f}}}\\
{\sum\limits_k^c {{c_{{\phi _1}k}}\frac{{\partial {\mu _k}}}{{\partial {{\bf{n}}_c}}}} }\\
 \vdots \\
{\sum\limits_k^c {{c_{{\phi _f}k}}\frac{{\partial {\mu _k}}}{{\partial {{\bf{n}}_c}}}} }
\end{array}} \right]
```

The dimensions of ${\bf{A}}$ are $c-r+f$ rows and $p$ columns. A matrix ${\bf{Z}}$ can be computed from ${\bf{A}}$ using either orthogonal or singular value decomposition that has the property of projecting the optimal parameters, ${{\bf{n}}_\phi}$, into the “linearized” null space of the constraints. The elements of ${\bf{Z}}$ are themselves functions of ${{\bf{n}}_\phi}$ because the last $f$ rows of ${\bf{A}}$ depend explicitly on ${{\bf{n}}_\phi}$. This is what makes the constraints non-linear in the optimal parameters. Nevertheless, ${\bf{A}}$ and ${\bf{Z}}$ can be applied to embody the non-linear constraints in an approximate fashion in order to develop an algorithm to compute the minimum of $\Lambda$.

Choose some initial guess for the minimum of $\Lambda$ that satisfies the equality constraints and call it ${\bf{n}}_{_\phi}^i$. Define a vector of differences, $\Delta {\bf{n}}_{_\phi}^i$, so that a better estimate for the minimum is given by ${\bf{n}}_\phi ^{i + 1} = \Delta {\bf{n}}_{_\phi }^i + {\bf{n}}_\phi ^i$. Applying the null space projection operator obtained from the linearized equality constraints,

```{math}
:label: eq29
{\bf{Zn}}_\phi ^{i + 1} = {\bf{Z}}\Delta {\bf{n}}_{_\phi }^i + {\bf{Zn}}_\phi ^i
```

$\Lambda$ may now be expanded in a Taylor series, as:

```{math}
:label: eq30
{\Lambda _{i + 1}} = {\Lambda _i} + {{\bf{g}}^T}{\bf{Z}}\Delta {\bf{n}}_\phi ^i + {\left( {\Delta {\bf{n}}_\phi ^i} \right)^T}{{\bf{Z}}^T}{\bf{WZ}}\Delta {\bf{n}}_\phi ^i +  \cdots
```

In Equation {eq}`eq30` ${\bf{g}}$ is the gradient of $L$ with respect to ${\bf{n}}_\phi$ evaluated at ${\bf{n}}_\phi ^i$ and ${\bf{W}}$ is a matrix of second derivatives of $\Lambda$ with respect to ${\bf{n}}_\phi$ again evaluated at ${\bf{n}}_\phi ^i$. This matrix is known as the Wronskian. Truncating the Taylor expansion after the quadratic term, differentiating the result with respect to ${\bf{n}}_\phi ^{i + 1}$ and setting the resultant expresser to zero, results in:

```{math}
:label: eq31
{{\bf{Z}}^T}{\bf{WZ}}\Delta {\bf{n}}_\phi ^i =  - {{\bf{Z}}^T}{\bf{g}}
```

Evaluation of ${\bf{W}}$ require estimates of the Lagrange multipliers. These estimates are provided by solving the system of equations:

```{math}
:label: eq32
{\bf{g}} = {{\bf{A}}^T}\left[ {\begin{array}{*{20}{c}}
{{\lambda _{{n_1}}}}\\
 \vdots \\
{{\lambda _{{n_{c - f}}}}}\\
{{\lambda _{{\phi _1}}}}\\
 \vdots \\
{{\lambda _{{\phi _f}}}}
\end{array}} \right]
```

The numerical solution to Equation {eq}`eq31` , $\Delta {\bf{n}}_{_\phi }^i$ is the quadratic approximation to the minimum of $\Lambda$. $\Delta {\bf{n}}_{_\phi }^i + {\bf{n}}_\phi ^i$ is not expected to yield the optimal solution, and in general $\Delta {\bf{n}}_{_\phi }^i$ does not result in an incremental approximation to the optimum that is feasible in light of the constraints. This is because the constraints have been linearized in order to compute $\Delta {\bf{n}}_{_\phi }^i$, and that linearization is exact only for infinitesimal values of $\Delta {\bf{n}}_{_\phi }^i$. Furthermore, the quadratic approximation to the minimum may require scaling to better locate a more optimal approximation to the mimimm, and usually this is accomplished by taking some step length, $s$, along the search direction, as:

```{math}
:label: eq33
{\bf{\tilde n}}_\phi ^{i + 1} = s\Delta {\bf{n}}_\phi ^i + {\bf{n}}_\phi ^i + \Delta {\bf{n}}_\phi ^{corr}
```

where $\Delta {\bf{n}}_\phi ^{corr}$ is a correction term that adjusts the result (${\bf{\tilde n}}_\phi ^{i + 1}$) to maintain feasibility of the constraints, i.e. Equation \ref{eq:27} . The correction term is generally small and must be computed iteratively.
