# Generalized thermodynamic potentials

The differential form of the Gibbs free energy ($G$) of a system may be written {cite}`prigogine1954treatise`:  

```{math}
:label: eq1
dG =  - SdT + VdP + \sum\limits_i^c {{\mu _i}d{n_i}}
```

where $T$, $P$, and $n_i$ are independent variables denoting the temperature, pressure and number of moles of the $i^{th}$ thermodynamic component in a system of $c$-components. The entropy of the system is given by $S$ ( = $- \frac{\partial G}{\partial T}$ ), the volume by $V$ ( $= \frac{\partial G}{\partial P}$ ) and the chemical potential of the $i^{th}$ component by $\mu_i$ ( $= - \frac{\partial G}{\partial {n_i}}$ ). The integral form of Equation {eq}`eq1` is:  
```{math}
:label: eq2
G = \sum\limits_i^c {\mu_i}{n_i}
```

The equilibrium state of a system is characterized by a minimum in the Gibbs free energy at specified temperature, pressure and bulk composition. Under these conditions the system evolves from a disequilibrium to a equilibrium state under the restriction that the system boundary is closed to mass transfer (constant $n_i$), but is open to heat exchange with its surroundings and to any volume change of the system associated with phase formation. Khorzhinskii {cite}`korzhinskiui1959physicochemical` generalized the concept of thermodynamic equilibrium to “open” systems subject to mass exchange with their surroundings for the specific case that the constancy of component mole number may be exchanged with constancy of the equivalent chemical potential, e.g. equilibrium in a system open to exchange of a fugitive component like H<sub>2</sub>O may be uniquely determined if the chemical potential of H<sub>2</sub>O is somehow externally imposed upon the system. The thermodynamic potential that is minimal under these “open” system conditions is the “Korzhinskii potential” {cite}`ghiorso1987evaluating`, which may be defined by a suitable Legendre transformation of Equation {eq}`eq2`, e.g., for the example involving H<sub>2</sub>O:

```{math}
:label: eq3
L = G - \frac{{\partial G}}{{\partial {n_{{{\rm{H}}_{\rm{2}}}{\rm{O}}}}}}{n_{{{\rm{H}}_{\rm{2}}}{\rm{O}}}} = G - {\mu _{{{\rm{H}}_{\rm{2}}}{\rm{O}}}}{n_{{{\rm{H}}_{\rm{2}}}{\rm{O}}}}
```

While the usual application of the Khorzhinskii potential is to the characterization of equilibrium in systems constrained by externally fixed chemical potentials, the theory equally applies to the case where a chemical potential, or some linear combination of chemical potentials, is constrained by the demand that a particular phase is present in the system. For example, if the system is forced to be in equilibrium with a pure H<sub>2</sub>O fluid phase at some temperature and pressure, then Equation {eq}`eq3` also defines the thermodynamic state function that is minimal at equilibrium for these conditions. Note, that the amount of fluid is not known a priori before minimizing $L$ subject to fixed $T$, $P$, ${n_{i \ne {{\rm{H}}_{\rm{2}}}{\rm{O}}}}$, and ${\mu _{{{\rm{H}}_{\rm{2}}}{\rm{O}}}}$; ${n_{{{\rm{H}}_{\rm{2}}}{\rm{O}}}}$ is a derived quantity.  

In general, for a phase of fixed stoichiometry [^footnote1], indexed as $\phi_j$, whose composition can be expressed using the system components vis-a-vis a reaction of the form:

```{math}
:label: eq4
\sum\limits_k^c {{c_{{\phi _j}k}}} componen{t_k} \Leftrightarrow phas{e_{{\phi _j}}}
```

the chemical potential of the phase is:

```{math}
:label: eq5
{\mu _{{\phi _j}}} = \sum\limits_k^c {{c_{{\phi _j}k}}{\mu _k}}
```

The Khorzhinskii potential that guarantees the presence of phase $\phi_j$ in the assemblage is:

```{math}
:label: eq6
L = G - \left( {\sum\limits_k^c {{c_{{\phi _j}k}}{n_k}} } \right)\left( {\sum\limits_k^c {{c_{{\phi _j}k}}{\mu _k}} } \right)
```

For a collection of *f*-phases, which are assumed to be present in the system, Equation {eq}`eq6` is generalized to:

```{math}
:label: eq7
L = G - \sum\limits_j^f {\left( {\sum\limits_k^c {{c_{{\phi _j}k}}{n_k}} } \right)\left( {\sum\limits_k^c {{c_{{\phi _j}k}}{\mu _k}} } \right)}
```

Equation {eq}`eq7` is the generalized Khorzhinskii potential for a thermodynamic system containing a specified collection of *f*-phases.  

If the system component mole numbers are collected into a vector, ${{\bf{n}}_c}$, and the coefficients ${{c_{{\phi _j}k}}}$ are made the components of a vector ${{\bf{R}}_{{\phi _j}}}$, then Equation {eq}`eq7` may be more compactly written as:

```{math}
:label: eq8
L = G - \sum\limits_j^f {{\bf{R}}_{{\phi _j}}^T{{\bf{n}}_c}{\bf{R}}_{{\phi _j}}^T\mu } 
```

[^footnote1]: A solution phase consisting of two or more stoichiometric end member components may be treated by writing a statement of Equation {eq}`eq4` for each end member; the amount of each end member, i.e. Equation {eq}`eq6`, thereafter determines the composition of the solution phase. See further discussion later in the text.
