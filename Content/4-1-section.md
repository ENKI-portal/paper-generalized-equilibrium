# Pure phase constraints

```{admonition} Jupyter notebook implementation
Follow this {doc}`link<../Notebooks/Example-pure-phases>` to a Jupyter notebook that implements this example.
```

We first consider the simple case explored previously involving two pure phases, quartz and corundum, in equilibrium with a silicate liquid. The Gibbs free energy of this system is given by:

```{math}
:label: eq34
G = n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} + n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}\mu _{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} + n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}\mu _{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq} + n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}\mu _{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}
```

and from Equation {eq}`eq23` the Khorzhinskii potential is:

```{math}
:label: eq35
L = n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} + n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}\mu _{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} + n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}\mu _{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq} + n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}\mu _{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq} - n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}
```

Equation {eq}`eq35` may be simplified to:

```{math}
:label: eq36
L = n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}\mu _{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} + n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}\mu _{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq} + n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}\mu _{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}
```

We now have a working definition for the Khorzhinskii potential.

In order to construct the Lagrangian associated with this potential, we first simply the equality constraint vector derived previously (Eq. {eq}`eq24`) by multiplying out the matrices and substituting liquid potentials for those of the system:

```{math}
:label: eq37
\left[ {\begin{array}{*{20}{c}}
{\frac{1}{2}n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq} - {b_{{{\rm{K}}_2}{\rm{O}}}}}\\
{n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq} - {b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}}\\
{n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} - {b_{{\rm{CaO}}}}}\\
{\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - \mu _{Qz}^o}\\
{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} - \mu _{Cr}^o}
\end{array}} \right]
```

The matrix $\bf{A}$ is the derivative of this vector with respect to $n_{{\rm{Si}}{{\rm{O}}_2}}^{liq},n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq},n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq},n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq},n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}$:

```{math}
:label: eq38
{\bf{A}} = \left[ {\begin{array}{*{20}{c}}
0&0&0&0&{\frac{1}{2}}\\
0&0&0&1&0\\
0&0&1&0&0\\
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}}&{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}}&{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}}}&{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq}}}}&{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}}}\\
{\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}}&{\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}}&{\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}}}&{\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq}}}}&{\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}}}
\end{array}} \right]
```

and the gradient, $\bf{g}$, of Equation {eq}`eq35` is:

```{math}
:label: eq39
{\bf{g}} = \frac{{dL}}{{d{{\bf{n}}_\phi }}} = \left[ {\begin{array}{*{20}{c}}
{\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}\\
{\mu _{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}\\
{\mu _{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}}\\
{\mu _{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}
\end{array}} \right] - \left[ {\begin{array}{*{20}{c}}
{\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}\\
0\\
0\\
0
\end{array}} \right] - n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\left[ {\begin{array}{*{20}{c}}
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}}}\\
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{CaSi}}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{KAlSi}}{{\rm{O}}_4}}}}}}
\end{array}} \right] - n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}\left[ {\begin{array}{*{20}{c}}
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}}}\\
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}}}}\\
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{CaSi}}{{\rm{O}}_3}}}}}}\\
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}}}}}\\
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{KAlSi}}{{\rm{O}}_4}}}}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
0\\
0\\
{\mu _{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}\\
{\mu _{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}}\\
{\mu _{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}
\end{array}} \right] - n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\left[ {\begin{array}{*{20}{c}}
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}}}\\
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{CaSi}}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{KAlSi}}{{\rm{O}}_4}}}}}}
\end{array}} \right] - n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}\left[ {\begin{array}{*{20}{c}}
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}}}\\
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}}}}\\
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{CaSi}}{{\rm{O}}_3}}}}}}\\
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}}}}}\\
{\frac{{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{KAlSi}}{{\rm{O}}_4}}}}}}
\end{array}} \right]
```

From Eq {eq}`eq39`, the Hessian of the Khorzhinskii function is 

```{math}
{\bf{H}} = \frac{{{d^2}L}}{{d{\bf{n}}_\phi ^2}} = \sum\limits_i^{} {\sum\limits_j^{} {\frac{{\partial G}}{{\partial {n_i}\partial {n_j}}}} }  - n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\sum\limits_i^{} {\sum\limits_j^{} {\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}\partial {n_j}}}} }  - n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}\sum\limits_i^{} {\sum\limits_j^{} {\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_i}\partial {n_j}}}} }
```

The Lagrange multiplier may now be obtained by solving Equation {eq}`eq32` using both Equation {eq}`eq38` and Equation {eq}`eq39` evaluated at an initial guess to the solution. How is this initial guess obtained? By specifying an initial system bulk composition, ${\bf{b}}_c$, and assigning that composition entirely to the liquid phase, at which point concentrations of $n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}$ and $n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}$ are adjusted to satisfy the equalities $\mu _{Qz}^o = \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}$ and $\mu _{Cr}^o = \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}$. Once the multipliers are computed the Lagrangian function,

```{math}
:label: eq40
\Lambda  = n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}\mu _{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} + n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}\mu _{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq} + n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}\mu _{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq} - {\lambda _{{{\rm{K}}_2}{\rm{O}}}}\left( {\frac{1}{2}n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq} - {b_{{{\rm{K}}_2}{\rm{O}}}}} \right) - {\lambda _{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}\left( {n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq} - {b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}} \right) - {\lambda _{{\rm{CaO}}}}\left( {n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} - {b_{{\rm{CaO}}}}} \right) - {\lambda _{{\rm{Qz}}}}\left( {\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - \mu _{Qz}^o} \right) - {\lambda _{{\rm{Cr}}}}\left( {\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} - \mu _{Cr}^o} \right)
```

may be evaluated and its Hessian computed by taking the second partial derivatives with respect to $n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}$, $n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}$, $n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}$, $n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq}$, $n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}$. For this purpose, Equation {eq}`eq40` is written more compactly as

```{math}
:label: eq41
\Lambda  = L - {\lambda _{{{\rm{K}}_2}{\rm{O}}}}\left( {\frac{1}{2}n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq} - {b_{{{\rm{K}}_2}{\rm{O}}}}} \right) - {\lambda _{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}\left( {n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq} - {b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}} \right) - {\lambda _{{\rm{CaO}}}}\left( {n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} - {b_{{\rm{CaO}}}}} \right) - {\lambda _{{\rm{Qz}}}}\left( {\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - \mu _{Qz}^o} \right) - {\lambda _{{\rm{Cr}}}}\left( {\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} - \mu _{Cr}^o} \right)
```

and its first derivative with respect to ${\bf{n}}_\phi$ is

```{math}
:label: eq42
\frac{{d\Lambda }}{{d{{\bf{n}}_\phi }}} = \frac{{dL}}{{d{{\bf{n}}_\phi }}} + \left[ {\begin{array}{*{20}{c}}
{ - {\lambda _{{\rm{Qz}}}}\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}} - {\lambda _{{\rm{Cr}}}}\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}}\\
{ - {\lambda _{{\rm{Qz}}}}\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}} - {\lambda _{{\rm{Cr}}}}\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}}\\
{ - {\lambda _{{\rm{CaO}}}} - {\lambda _{{\rm{Qz}}}}\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}} - {\lambda _{{\rm{Cr}}}}\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}}}\\
{ - {\lambda _{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}} - {\lambda _{{\rm{Qz}}}}\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}}} - {\lambda _{{\rm{Cr}}}}\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}}}}\\
{ - {\lambda _{{{\rm{K}}_2}{\rm{O}}}} - {\lambda _{{\rm{Qz}}}}\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}} - {\lambda _{{\rm{Cr}}}}\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}}}
\end{array}} \right]
```

which yields a second derivative

```{math}
:label: eq43
\frac{{{\partial ^2}\Lambda }}{{\partial {n_i}\partial {n_j}}} = \frac{{\partial G}}{{\partial {n_i}\partial {n_j}}} - n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}\partial {n_j}}} - n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}\frac{{\partial \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_i}\partial {n_j}}} - {\lambda _{{\rm{Qz}}}}\frac{{{\partial ^2}\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}\partial {n_j}}} - {\lambda _{{\rm{Cr}}}}\frac{{{\partial ^2}\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}}{{\partial {n_i}\partial {n_j}}}
```

The elements of the Hessian are given by Equation {eq}`eq43`.

In order to compute a search direction according to Equation {eq}`eq31` the orthogonal null space projection matrix, $\bf{Z}$, must be computed from $\bf{A}$. Previously we obtained a similar matrix to project the equality constraints by employing Singular Value Decomposition. That numerical technique is computationally costly, and its use is acceptable because the projection only needed to be applied once for the problem at hand. However, $\bf{A}$ must be evaluated and decomposed numerous times in the course of minimizing the Lagrangian, so the fastest possible numerical algorithm should be employed. The preferred method is RQ decomposition, which factors the matrix into the product:

```{math}
:label: eq44
{\bf{A}} = {\bf{RQ}} = \left[ {\begin{array}{*{20}{c}}
{{{\bf{R}}_1}_1}&{\bf{0}}
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{{{\bf{Q}}_1}}\\
{{{\bf{Q}}_2}}
\end{array}} \right]
```

where the matrix $\bf{Q}$ is orthonormal (${{\bf{Q}}^T}{\bf{Q}} = {\bf{I}}$) and $\bf{R}$ is upper triangular. From Equation {eq}`eq44` it follows that ${{\bf{Q}}_2}^T{\bf{A}} = {\bf{0}}$ , and consequently that ${{\bf{Q}}_2}^T$ is the $\bf{Z}$ matrix that we seek.
