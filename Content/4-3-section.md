# Fixed saturation with a solid solution

```{admonition} Jupyter notebook implementation
Follow this {doc}`link<../Notebooks/Example-solutions>` to a Jupyter notebook that implements this example.
```

We next consider the case of one pure phase, quartz, and a solid solution, feldspar, in equilibrium with a silicate liquid. The Gibbs free energy of this system is given by

```{math}
:label: eq61
G = {G^{liq}}\left( {n_{{\rm{Si}}{{\rm{O}}_2}}^{liq},n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq},n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq},n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq},n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}} \right)
```

and the Khorzhinskii potential by

```{math}
:label: eq62
L = {G^{liq}}\left( {n_{{\rm{Si}}{{\rm{O}}_2}}^{liq},n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq},n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq},n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq},n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}} \right) - n_{Qz}^{liq}\mu _{Qz}^{liq} - n_{Ab}^{liq}\Omega _{Ab}^{liq} - n_{An}^{liq}\Omega _{An}^{liq} - n_{Sn}^{liq}\Omega _{Sn}^{liq}
```

where

```{math}
:label: eq63
\begin{array}{*{20}{c}}
{\begin{array}{*{20}{l}}
{\Omega _{Ab}^{liq} = \frac{5}{2}\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + \frac{1}{2}\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} + \frac{1}{2}\mu _{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}}\\
{\Omega _{An}^{liq} = \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} + \frac{1}{2}\mu _{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}\\
{\Omega _{Sn}^{liq} = 2\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + \mu _{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}
\end{array}}\\
\begin{array}{l}
n_{Ab}^{liq} = \frac{5}{2}n_{{\rm{Si}}{{\rm{O}}_2}}^{liq} + \frac{1}{2}n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} + \frac{1}{2}n_{{\rm{N}}{{\rm{a}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_3}}^{liq}\\
n_{An}^{liq} = n_{{\rm{Si}}{{\rm{O}}_2}}^{liq} + n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} + \frac{1}{2}n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}\\
n_{Sn}^{liq} = 2n_{{\rm{Si}}{{\rm{O}}_2}}^{liq} + n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}
\end{array}
\end{array}
```

The equality constraint matrix for this problem is

```{math}
:label: eq64
\left[ {\begin{array}{*{20}{c}}
{{b_{{\rm{Si}}{{\rm{O}}_2}}}}\\
{{b_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}}\\
{{b_{{\rm{CaO}}}}}\\
{{b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}}\\
{{b_{{{\rm{K}}_2}{\rm{O}}}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
1&0&1&1&1&1&3&2&3\\
0&1&0&0&{\frac{1}{2}}&0&{\frac{1}{2}}&1&{\frac{1}{2}}\\
0&0&1&0&0&0&0&1&0\\
0&0&0&1&0&0&{\frac{1}{2}}&0&0\\
0&0&0&0&{\frac{1}{2}}&0&0&0&{\frac{1}{2}}
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}\\
{n_{Qz}^{liq}}\\
{n_{Ab}^{liq}}\\
{n_{An}^{liq}}\\
{n_{Sn}^{liq}}
\end{array}} \right]
```

and the decomposition of the "fixed" constraint partition of the constraint matrix is given by:

```{math}
:label: eq65
{\bf{C}}_f^T = \left[ {\begin{array}{*{20}{c}}
1&0&0&0&0\\
3&{\frac{1}{2}}&0&{\frac{1}{2}}&0\\
2&1&1&0&0\\
3&{\frac{1}{2}}&0&0&{\frac{1}{2}}
\end{array}} \right] = {{\bf{U}}_f}{{\bf{S}}_f}{\bf{V}}_f^T = {{\bf{U}}_f}\left[ {\begin{array}{*{20}{c}}
x&0&0&0&0\\
0&x&0&0&0\\
0&0&x&0&0\\
0&0&0&x&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
x&x&x&x&x\\
x&x&x&x&x\\
0&0&0&x&x\\
x&x&x&x&x\\
0&{ - \frac{1}{2}}&{\frac{1}{2}}&{\frac{1}{2}}&{\frac{1}{2}}
\end{array}} \right]
```

where $x$ denotes some non-zero entry. The null space projection operator applied to the contraint matrix, Equation {eq}`eq63`, gives:

```{math}
:label: eq66
\left[ {\begin{array}{*{20}{c}}
0&{ - \frac{1}{2}}&{\frac{1}{2}}&{\frac{1}{2}}&{\frac{1}{2}}
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{{b_{{\rm{Si}}{{\rm{O}}_2}}}}\\
{{b_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}}\\
{{b_{{\rm{CaO}}}}}\\
{{b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}}\\
{{b_{{{\rm{K}}_2}{\rm{O}}}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
0&{ - \frac{1}{2}}&{\frac{1}{2}}&{\frac{1}{2}}&{\frac{1}{2}}
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
1&0&1&1&1\\
0&1&0&0&{\frac{1}{2}}\\
0&0&1&0&0\\
0&0&0&1&0\\
0&0&0&0&{\frac{1}{2}}
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}
\end{array}} \right]
```

which simplifies to

```{math}
:label: eq67
\frac{1}{2}\left( {{b_{{\rm{CaO}}}} + {b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}} + {b_{{{\rm{K}}_2}{\rm{O}}}} - {b_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}} \right) = \left[ {\begin{array}{*{20}{c}}
0&{ - \frac{1}{2}}&{\frac{1}{2}}&{\frac{1}{2}}&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}
\end{array}} \right]
```

or simply the one constraint:

```{math}
:label: eq68
\frac{1}{2}\left( {{b_{{\rm{CaO}}}} + {b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}} + {b_{{{\rm{K}}_2}{\rm{O}}}} - {b_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}} \right) = \frac{1}{2}\left( {n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} + n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq} - n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}} \right)
```

The complete set of constraints is now written:

```{math}
:label: eq69
\begin{array}{*{20}{c}}
{\frac{1}{2}\left( {n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} + n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq} - n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}} \right) - \frac{1}{2}\left( {{b_{{\rm{CaO}}}} + {b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}} + {b_{{{\rm{K}}_2}{\rm{O}}}} - {b_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}} \right) = 0}\\
{\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - \mu _{{\rm{Si}}{{\rm{O}}_2}}^{qtz} = 0}\\
{\Omega _{Ab}^{liq} - \mu _{Ab}^{plag} = 0}\\
{\Omega _{An}^{liq} - \mu _{An}^{plag} = 0}\\
{\Omega _{Sn}^{liq} - \mu _{Sn}^{plag} = 0}
\end{array}
```

Even without formally writing down definitions of the $\bf{A}$ matrix, gradient vector or the Lagrangian, if follows that if the chemical of the feldspars is specified as a constraint as in Equation {eq}`eq69`, then the solution is uniquely determined without the need for potential minimization.

The gradient of the Khorzhinskii potential is computed from Equation {eq}`eq62` as

```{math}
:label: eq70
\frac{{\partial L}}{{\partial {n_i}}} = \frac{{\partial G}}{{\partial {n_i}}} - \frac{{\partial n_{Qz}^{liq}}}{{\partial {n_i}}}\mu _{Qz}^{liq} - n_{Qz}^{liq}\frac{{\partial \mu _{Qz}^{liq}}}{{\partial {n_i}}} - \frac{{\partial n_{Ab}^{liq}}}{{\partial {n_i}}}\Omega _{Ab}^{liq} - n_{Ab}^{liq}\frac{{\partial \Omega _{Ab}^{liq}}}{{\partial {n_i}}} - \frac{{\partial n_{An}^{liq}}}{{\partial {n_i}}}\Omega _{An}^{liq} - n_{An}^{liq}\frac{{\partial \Omega _{An}^{liq}}}{{\partial {n_i}}} - \frac{{\partial n_{Sn}^{liq}}}{{\partial {n_i}}}\Omega _{Sn}^{liq} - n_{Sn}^{liq}\frac{{\partial \Omega _{Sn}^{liq}}}{{\partial {n_i}}}
```

The Lagrangian function is:

```{math}
:label: eq71
\Lambda  = L - {\lambda _b}\left[ {\frac{1}{2}\left( {n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} + n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq} - n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}} \right) - \frac{1}{2}\left( {{b_{{\rm{CaO}}}} + {b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}} + {b_{{{\rm{K}}_2}{\rm{O}}}} - {b_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}} \right)} \right] - {\lambda _{Qz}}\left( {\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - \mu _{{\rm{Si}}{{\rm{O}}_2}}^{qtz}} \right) - \lambda _{Ab}^{fld}\left( {\Omega _{Ab}^{liq} - \mu _{Ab}^{fld}} \right) - \lambda _{An}^{fld}\left( {\Omega _{An}^{liq} - \mu _{An}^{fld}} \right) - \lambda _{Sn}^{fld}\left( {\Omega _{Sn}^{liq} - \mu _{Sn}^{fld}} \right)
```

which has a gradient:

```{math}
:label: eq72
\frac{{\partial \Lambda }}{{\partial {n_i}}} = \frac{{\partial L}}{{\partial {n_i}}} - {\lambda _b}\frac{1}{2}\left( {\delta _{i,{\rm{CaSi}}{{\rm{O}}_3}}^{liq} + \delta _{i,{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq} - \delta _{i,{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}} \right) - {\lambda _{Qz}}\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}}} - \lambda _{Ab}^{fld}\frac{{\partial \Omega _{Ab}^{liq}}}{{\partial {n_i}}} - \lambda _{An}^{fld}\frac{{\partial \Omega _{An}^{liq}}}{{\partial {n_i}}} - \lambda _{Sn}^{fld}\frac{{\partial \Omega _{Sn}^{liq}}}{{\partial {n_i}}}
```

from which the elements of the second derivative are:

```{math}
:label: eq73
\frac{{{\partial ^2}\Lambda }}{{\partial {n_i}\partial {n_j}}} = \frac{{{\partial ^2}L}}{{\partial {n_i}\partial {n_j}}} - {\lambda _{Qz}}\frac{{{\partial ^2}\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}\partial {n_j}}} - \lambda _{Ab}^{fld}\frac{{{\partial ^2}\Omega _{Ab}^{liq}}}{{\partial {n_i}\partial {n_j}}} - \lambda _{An}^{fld}\frac{{{\partial ^2}\Omega _{An}^{liq}}}{{\partial {n_i}\partial {n_j}}} - \lambda _{Sn}^{fld}\frac{{{\partial ^2}\Omega _{Sn}^{liq}}}{{\partial {n_i}\partial {n_j}}}
```

