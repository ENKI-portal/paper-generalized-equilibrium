# Extra material to describe derivatives

Khorzhinskii potential:  

$L = G\left( {{\bf{n}},T,P} \right) - \left( {{{\bf{r}}^T}{\bf{n}}} \right)\Phi \left( {{\bf{n}},T,P} \right)$  

$\frac{{\partial L}}{{\partial {\bf{n}}}} = \frac{{\partial G\left( {{\bf{n}},T,P} \right)}}{{\partial {\bf{n}}}} - \left( {{{\bf{r}}^T}\frac{{\partial {\bf{n}}}}{{\partial {\bf{n}}}} + {\bf{n}}\frac{{\partial {{\bf{r}}^T}}}{{\partial {\bf{n}}}}} \right)\Phi \left( {{\bf{n}},T,P} \right) - \left( {{{\bf{r}}^T}{\bf{n}}} \right)\frac{{\partial \Phi \left( {{\bf{n}},T,P} \right)}}{{\partial {\bf{n}}}}$  

note 

$\frac{{\partial {\bf{n}}}}{{\partial {\bf{n}}}} = {\bf{I}}$, $\frac{{\partial {{\bf{r}}^T}}}{{\partial {\bf{n}}}} = {\bf{0}}$, ${{\bf{r}}^T}{\bf{I}} = {\bf{r}}$  

$\frac{{\partial L}}{{\partial {\bf{n}}}} = \frac{{\partial G\left( {{\bf{n}},T,P} \right)}}{{\partial {\bf{n}}}} - {\bf{r}}\Phi \left( {{\bf{n}},T,P} \right) - \left( {{{\bf{r}}^T}{\bf{n}}} \right)\frac{{\partial \Phi \left( {{\bf{n}},T,P} \right)}}{{\partial {\bf{n}}}}$  

$\frac{{{\partial ^2}L}}{{\partial {{\bf{n}}^2}}} = \frac{{{\partial ^2}G\left( {{\bf{n}},T,P} \right)}}{{\partial {{\bf{n}}^2}}} - \frac{{\partial \Phi \left( {{\bf{n}},T,P} \right)}}{{\partial {\bf{n}}}}{{\bf{r}}^T} - {\bf{r}}\frac{{\partial \Phi \left( {{\bf{n}},T,P} \right)}}{{\partial {\bf{n}}}} - \left( {{{\bf{r}}^T}{\bf{n}}} \right)\frac{{{\partial ^2}\Phi \left( {{\bf{n}},T,P} \right)}}{{\partial {{\bf{n}}^2}}}$  

The Lagrangian:  

$\Lambda  = L - \lambda \left( {\Phi \left( {{\bf{n}},T,P} \right) - {\Phi ^{fix}}\left( {T,P} \right)} \right)$  

$\frac{{{\partial ^2}\Lambda }}{{\partial {{\bf{n}}^2}}} =  - \lambda \frac{{{\partial ^2}\Phi \left( {{\bf{n}},T,P} \right)}}{{\partial {{\bf{n}}^2}}}$  

Constraint derivatives:  

$\Phi \left( {{\bf{n}},T,P} \right) = {{\bf{r}}^T}\frac{{\partial G\left( {{\bf{n}},T,P} \right)}}{{\partial {\bf{n}}}}$  
$\frac{{\partial \Phi \left( {{\bf{n}},T,P} \right)}}{{\partial {\bf{n}}}} = \frac{{{\partial ^2}G\left( {{\bf{n}},T,P} \right)}}{{\partial {{\bf{n}}^2}}}{\bf{r}}$

