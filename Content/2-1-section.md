# Example of phase constraints

Consider a chemical system with thermodynamic components SiO<sub>2</sub>, Al<sub>2</sub>O<sub>3</sub>, CaO, Na<sub>2</sub>O and K<sub>2</sub>O. For this system we are interested in phase equilibria at some specified temperature and pressure assuming that both quartz (SiO<sub>2</sub>) and corundum (Al<sub>2</sub>O<sub>3</sub>) are present in the system. In addition, the system is known to contain a silicate liquid. We do not know the composition of the silicate liquid, but we have a thermodynamic model that describes its Gibbs free energy of this solution in terms of components SiO<sub>2</sub>, Al<sub>2</sub>O<sub>3</sub>, CaSiO<sub>3</sub>, Na<sub>2</sub>SiO<sub>3</sub>, KAlSiO<sub>4</sub>. The system is free to exchange both SiO<sub>2</sub> and Al<sub>2</sub>O<sub>3</sub> with its surroundings to alter the initially specified bulk composition, given by

```{math}
:label: eq17
{{\bf{b}}_c} = {\left[ {\begin{array}{*{20}{c}}
{{b_{{\rm{Si}}{{\rm{O}}_2}}}}&{{b_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}}&{{b_{{\rm{CaO}}}}}&{{b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}}&{{b_{{{\rm{K}}_2}{\rm{O}}}}}
\end{array}} \right]^T}
```

in order to bring the system to equilibrium with quartz and corundum at the specified temperature and pressure.

Equation {eq}`eq15` may be written for this case as:

```{math}
:label: eq18
\left[ {\begin{array}{*{20}{c}}
{{b_{{\rm{Si}}{{\rm{O}}_2}}}}\\
{{b_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}}\\
{{b_{{\rm{CaO}}}}}\\
{{b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}}\\
{{b_{{{\rm{K}}_2}{\rm{O}}}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
1&0\\
0&1\\
0&0\\
0&0\\
0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{{n_{Qz}}}\\
{{n_{Cr}}}
\end{array}} \right] + \left[ {\begin{array}{*{20}{c}}
1&0&1&1&1\\
0&1&0&0&{\frac{1}{2}}\\
0&0&1&0&0\\
0&0&0&1&0\\
0&0&0&0&{\frac{1}{2}}
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}
\end{array}} \right]
```

and Equation {eq}`eq13` becomes:

```{math}
:label: eq19
{\bf{C}}_f^T = \left[ {\begin{array}{*{20}{c}}
1&0&0&0&0\\
0&1&0&0&0
\end{array}} \right] = {{\bf{U}}_f}{{\bf{S}}_f}{\bf{V}}_f^T = \left[ {\begin{array}{*{20}{c}}
0&1\\
1&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
1&0&0&0&0\\
0&1&0&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
0&1&0&0&0\\
1&0&0&0&0\\
0&0&0&0&1\\
0&0&0&1&0\\
0&0&1&0&0
\end{array}} \right]
```

from which

```{math}
:label: eq20
V_{\left. f \right|f}^T = \left[ {\begin{array}{*{20}{c}}
0&0&0&0&1\\
0&0&0&1&0\\
0&0&1&0&0
\end{array}} \right]
```

Multiplying Equation {eq}`eq17` by Equation {eq}`eq19` gives

```{math}
:label: eq21
\left[ {\begin{array}{*{20}{c}}
0&0&0&0&1\\
0&0&0&1&0\\
0&0&1&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{{b_{{\rm{Si}}{{\rm{O}}_2}}}}\\
{{b_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}}}\\
{{b_{{\rm{CaO}}}}}\\
{{b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}}\\
{{b_{{{\rm{K}}_2}{\rm{O}}}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
0&0&0&0&1\\
0&0&0&1&0\\
0&0&1&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
1&0\\
0&1\\
0&0\\
0&0\\
0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{{n_{Qz}}}\\
{{n_{Cr}}}
\end{array}} \right] + \left[ {\begin{array}{*{20}{c}}
0&0&0&0&1\\
0&0&0&1&0\\
0&0&1&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
1&0&1&1&1\\
0&1&0&0&{\frac{1}{2}}\\
0&0&1&0&0\\
0&0&0&1&0\\
0&0&0&0&{\frac{1}{2}}
\end{array}} \right]
```

which simplified to

```{math}
:label: eq22
\left[ {\begin{array}{*{20}{c}}
{{b_{{{\rm{K}}_2}{\rm{O}}}}}\\
{{b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}}\\
{{b_{{\rm{CaO}}}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
0&0\\
0&0\\
0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{{n_{Qz}}}\\
{{n_{Cr}}}
\end{array}} \right] + \left[ {\begin{array}{*{20}{c}}
0&0&0&0&{\frac{1}{2}}\\
0&0&0&1&0\\
0&0&1&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{}}
\end{array}} \right]
```

Equation {eq}`eq22` is a concrete example of Equation {eq}`eq16`. Note that the final set of constraints do not involve the initial concentrations of SiO<sub>2</sub> or Al<sub>2</sub>O<sub>3</sub> in the system, which is consistent with the open system nature of the phase present constraints.

To complete this example, note the the Khorzhinskii potential, Equation {eq}`eq8`, would be written:

```{math}
:label: eq23
L\left( {T,P,n_{{\rm{Si}}{{\rm{O}}_2}}^{liq},n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq},n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq},n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq},n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}} \right) = G - n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}
```

and that to compute thermodynamic equilibrium, this function would be minimized subject to constant temperature, pressure, and

```{math}
:label: eq24
\begin{array}{c}
\left[ {\begin{array}{*{20}{c}}
0&0\\
0&0\\
0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{{n_{Qz}}}\\
{{n_{Cr}}}
\end{array}} \right] + \left[ {\begin{array}{*{20}{c}}
0&0&0&0&{\frac{1}{2}}\\
0&0&0&1&0\\
0&0&1&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{CaSi}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq}}
\end{array}} \right] - \left[ {\begin{array}{*{20}{c}}
{{b_{{{\rm{K}}_2}{\rm{O}}}}}\\
{{b_{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}}}\\
{{b_{{\rm{CaO}}}}}
\end{array}} \right] = 0\\
\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - \mu _{Qz}^o = 0\\
\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq} - \mu _{_{Cr}}^o = 0
\end{array}
```

Finally, note that because the liquid is an omnicomponent phase, the system chemical potentials can be written in terms of liquid components:

```{math}
:label: eq25
\begin{array}{c}
{\mu _{{\rm{Si}}{{\rm{O}}_2}}} = \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}\\
{\mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}} = \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}\\
{\mu _{{\rm{CaO}}}} = \mu _{{\rm{CaSi}}{{\rm{O}}_3}}^{liq} - \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}\\
{\mu _{{\rm{N}}{{\rm{a}}_2}{\rm{O}}}} = \mu _{{\rm{N}}{{\rm{a}}_2}{\rm{Si}}{{\rm{O}}_3}}^{liq} - \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}\\
{\mu _{{{\rm{K}}_2}{\rm{O}}}} = 2\mu _{{\rm{KAlSi}}{{\rm{O}}_4}}^{liq} - 2\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - \mu _{{\rm{A}}{{\rm{l}}_2}{{\rm{O}}_3}}^{liq}
\end{array}
```

In the next section, we consider numerical methods of performing this minimization.
