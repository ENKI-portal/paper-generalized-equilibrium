# Constraints on the minimum

In order to determine the equilibrium phase assemblage governed by the potential of Equation {eq}`eq7`, temperature, pressure and the mass of all system components that are not allowed to vary by phase present constraints must be specified. In addition, constraints on chemical potentials, i.e., Equation {eq}`eq5`, must be imposed in order to guarantee that the stipulated phase assemblage is present at equilibrium. Without the phase present constraints, the specification of a constraint matrix that guarantees constancy of system bulk composition is straightforward, and results in a linear system of equality constraints on the mole numbers of phases in the equilibrium assemblage {cite}`ISI:A1985ALE5400002`. For this phase-present scenario, these constraints become more involved.

Consider a system of $p$-phases at some specified temperature and pressure, with $f$ of those $p$ phases specified or forced to be present in the system. The number of “extra” or non-specified phases is $e \equiv p - f \ge 0$. For the moment, we assume that the identity of the $e$ phases in the system is known[^footnote3]. At fixed temperature and pressure, the Gibbs phase rule guarantees that $c \ge p = f + e$.

There always exists a linear mapping between the mole numbers of phases present in the system and the mole of the $k^{th}$ system component:

```{math}
:label: eq10
{n_k} = \sum\limits_j^p {{n_{{\phi _j}}}c_{k{\phi_j}}^*}
```

This mapping can be extended to all components and succinctly written in matrix form:

```{math}
:label: eq11
{{\bf{n}}_c} = \left[ {\begin{array}{*{20}{c}}
{{n_1}}\\
 \vdots \\
{{n_k}}\\
 \vdots \\
{{n_c}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
{c_{1{\phi _1}}^*}& \cdots &{c_{1{\phi _j}}^*}& \cdots &{c_{1{\phi _p}}^*}\\
 \vdots & \ddots & \vdots & {\mathinner{\mkern2mu\raise1pt\hbox{.}\mkern2mu
 \raise4pt\hbox{.}\mkern2mu\raise7pt\hbox{.}\mkern1mu}} & \vdots \\
{c_{k{\phi _1}}^*}& \cdots &{c_{k{\phi _j}}^*}& \cdots &{c_{k{\phi _p}}^*}\\
 \vdots & {\mathinner{\mkern2mu\raise1pt\hbox{.}\mkern2mu
 \raise4pt\hbox{.}\mkern2mu\raise7pt\hbox{.}\mkern1mu}} & \vdots & \ddots & \vdots \\
{c_{c{\phi _1}}^*}& \cdots &{c_{c{\phi _j}}^*}& \cdots &{c_{c{\phi _p}}^*}
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{{n_{{\phi _1}}}}\\
 \vdots \\
{{n_{{\phi _k}}}}\\
 \vdots \\
{{n_{{\phi _p}}}}
\end{array}} \right] = {\bf{C}}{{\bf{n}}_{\phi}}
```

where the column dimension of $\bf{C}$ is greater than or equal to its row dimension. The matrix $\bf{C}$ may be partitioned as $\left[ {{{\bf{C}}_f}|{{\bf{C}}_e}} \right]$ , and Equation {eq}`eq11` can be re-written

```{math}
:label: eq12
{\bf{n}_c} = {{\bf{C}}_f}{{\bf{n}}_f} + {{\bf{C}}_e}{{\bf{n}}_e}
```

Note that ${{\bf{n}}_\phi }$, the vector of mole numbers of all phases in the system[^footnote2], is partitioned as ${\left[ {\begin{array}{*{20}{c}}{{{\bf{n}}_f}}&{{{\bf{n}}_e}}\end{array}} \right]^T}$. The matrix ${\bf{C}}_f$ may have rank less than $f$. Any number of situations can generate this rank deficiency, including imposing a collection of fixed phases whose compositions are redundant, such as multiple structural forms of the same stoichiometry. The transpose of the matrix ${\bf{C}}_f$ may be rendered into the product of three matrices using Singular Value Decomposition {cite}`lawson1995solving`, as:

```{math}
:label: eq13
{\bf{C}}_f^T = {{\bf{U}}_f}{{\bf{S}}_f}{\bf{V}}_f^T
```

where ${\bf{U}}_f$ and ${\bf{V}}_f$ are square orthogonal matrices and ${\bf{S}}_f$ is a diagonal-dominant rectangular matrix; the number of non-zero diagonal elements of ${\bf{S}}_f$ is equal to the rank, $r$, of ${\bf{C}}_f$. The columns of ${\bf{V}}_f$, which are ordered to pair with the diagonal elements of ${\bf{S}}_f$, partition into an $r$ column range space and $f - r$ column null space:

```{math}
:label: eq14
\left[ {\begin{array}{*{20}{c}}
{{v_{11}}}& \cdots &{{v_{1r}}}&{{v_{1r + 1}}}& \cdots &{{v_{1f}}}\\
 \vdots & \cdots & \vdots & \vdots & \cdots & \vdots \\
{{v_{k1}}}& \cdots &{{v_{kr}}}&{{v_{kr + 1}}}& \cdots &{{v_{kf}}}\\
 \vdots & \cdots & \vdots & \vdots & \cdots & \vdots \\
{{v_{c1}}}& \cdots &{{v_{cr}}}&{{v_{cr + 1}}}& \cdots &{{v_{cf}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
{{{\bf{v}}_1}}& \cdots &{{{\bf{v}}_r}}&{{{\bf{v}}_{r + 1}}}& \cdots &{{{\bf{v}}_f}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
{{{\bf{V}}_{\left. f \right|r}}}&{{{\bf{V}}_{\left. f \right|f}}}
\end{array}} \right]
```

The range space defines the set of linear combinations of system component mole numbers that are free to assume any value in order to accommodate the forced equilibration of the system with the $f$ fixed phases. The null space defines those linear combinations of system component mole numbers are fixed — i.e., that are independent of any mass transfer associated with bringing the system into equilibrium with the imposed assemblage. If ${\bf{b}}_c$ denotes a vector of mole numbers of system components, then if ${\bf{b}}_c$ is substituted for the left hand side of {eq}`eq12`,

```{math}
:label: eq15
{\bf{b}_c} = {{\bf{C}}_f}{{\bf{n}}_f} + {{\bf{C}}_e}{{\bf{n}}_e}
```

and both sides of the expression are multiplied by ${\bf{V}}_{\left. f \right|f}^T$, the result is a set of constraints that permit partial mass exchange according to the stoichiometry of the fixed phase constraints, yet maintain initial system composition in the null space of those phase constraints:

```{math}
:label: eq16
{\bf{V}}_{\left. f \right|f}^T{{\bf{b}}_{\bf{c}}} = {\bf{V}}_{\left. f \right|f}^T{{\bf{C}}_f}{{\bf{n}}_f} + {\bf{V}}_{\left. f \right|f}^T{{\bf{C}}_e}{{\bf{n}}_e}
```
At this point a brief concrete example is welcome and illustrative.

[^footnote2]: If a phase is a solution, there is an entry in the vector for each end member component of that solution.
[^footnote3]: {cite}`ISI:000313688600019` describes algorithms for estimating the phase identity and approximate phase compositions for systems where the final phase assemblage requires estimation.