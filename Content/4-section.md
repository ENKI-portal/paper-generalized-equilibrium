# Case Studies

In this section we present a few illustrations of solving various phase-constrained equilibrium calculations using the methods developed above.
