# Calculating Equilibrium Phase Assemblages in Systems Subject to Generalized Constraints on System Chemical Potentials

## Introduction

This paper is concerned with the theory of energy potentials that are minimal in open thermodynamic systems whose boundary mass transfer is constrained by the imposition of fixed, possibly externally imposed, chemical potentials. This paper also strives to pesent a rigorous computational thermodynamic framework for minimization of these potentials with the aim of practical computation of phase equilibria.

The theory presented here augments that of Korzhinskii {cite:p}`korzhinskiui1959physicochemical` who first generalized the concept of the Gibbs phase rule to open systems.  The phase rule itself is rooted in the fundamental differential equation{cite:p}`Gibbs-1`,{cite:p}`Gibbs-2` for the chemical evolution of a thermodynamic system,

```{math}
:label: intro1
-SdT+VdP-\sum_i^cn_id\mu_i=0
```

which demonstrates that constancy of the intensive thermodynamic variables, namely temperature (*T*), pressure (*P*), and component chemical potentials ($\mu_i$) characterize a stationary or equilibrium state[^footnote0]. Equation {eq}`intro1` may be obtained by Legendre transformations{cite:p}`callen` of the combined first and second laws of thermodynamics cast into a form where only expansion work is considered and the disequilibrium contribution to the generation of irreversible heat (i.e., the chemical affinity) is expressed solely by chemical reactions:

```{math}
:label: intro2
dE = TdS - PdV + \sum\limits_i^c {{\mu _i}d{n_i}}
```

Equation {eq}`intro1` represents a complete Lengedre transformation of equation {eq}`intro2` such that every extensive (mass dependent) differential element is transformed to its intensive equivalent. In essence, the Korzhinskii method that we will follow in this paper constitutes partial Legendre transformations of Equation {eq}`intro2` where some of the extensive compositional variables (i.e., the $n_i$ terms) are replaced by equivalent differentials in chemical potential. The transformed differential equation is then utilized to calculate an equilibrium state by imposing values of chemical potentials on the system.  These values are fixed by externally imposed constraints or by specification of the presence of phases in the system that directly bound linear combinations of the chemical potentials. By design, these partial Legendre transformations generate differential quations that *do not* represent a minimum in the Gibbs free energy of the system, or for that matter any of the standard thermodynamic potentials (e.g., internal energy, enthalpy or Helmholtz free energy) *unless the system already satisfies the imposed chemical potential constraints*. Minimization of the system potential that is consistent with a partial Legnedre transformation of the compositional terms in Equation {eq}`intro2` requires exchange of mass across the system boundary. These thermodynamic potentials characterize euilibrium in *open* thermodynamic systems that are subject to *generalized constraints on chemical potentials*. 

Before proceeding to the theoretical development of these open system potentials, it is important to consider one additional consequence of opening the system boundary to selective mass exchange. In a closed system Duhem's theorem {cite:p}`duhem1` states that equilibrium can be fully specified using two free thermodynamic variables, e.g., (*T*, *P*), (*T*, *V*), etc., regardless of the specific identity of phases in the system. This is not the case in an open system subject to generalized chemical potential constraints.  Under these circumstances the number of variables that must be specified is two plus the number of imposed constraints. This result may seem surprising, but it follows immediately from Equation {eq}`intro1`, which demonstrates that all chemical potentials as well as *T* and *P* must not vary in the equilibrium state.  If that set of chemical potentials is not implicitly constrained by fixing temperature, pressure and component masses, as in the closed system case, then the fugitive component must have its chemical potential fixed by a constraint imposed at the boundaries of the system.

In the first part of this paper we develop the theory behind generalized thermodynamic potentials and in the remainder of the paper we illustrate case studies and provide petrologic applications.

[^footnote0]: *S* denotes the entropy, *V* the volume, $n_i$ the mole numbers of components, and *E* the internal energy of the system. 

