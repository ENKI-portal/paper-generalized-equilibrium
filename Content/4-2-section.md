# Fixed chemical potential of oxygen and water

```{admonition} Jupyter notebook implementation
Follow this {doc}`link<../Notebooks/Example-oxygen-water>` to a Jupyter notebook that implements this example.
```

Next we consider a silicate liquid in the system SiO<sub>2</sub>-Fe<sub>2</sub>O<sub>3</sub>-Fe<sub>2</sub>SiO<sub>4</sub>-H<sub>2</sub>O. The problem is similar to the one in the previous section, except that it is assumed that an O<sub>2</sub>-H<sub>2</sub>O fluid phase is not saturated. The Gibbs free energy of this system is written as:

```{math}
:label: eq45
G = n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq}\mu _{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq} + n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}\mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq} + n_{{{\rm{H}}_2}{\rm{O}}}^{liq}\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}
```

from which the Khorzhinskii potential is formally given by

```{math}
:label: eq46
L = n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq}\mu _{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq} + n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}\mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq} + n_{{{\rm{H}}_2}{\rm{O}}}^{liq}\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq} - n_{{{\rm{O}}_2}}^{liq}\mu _{{{\rm{O}}_2}}^{liq} - n_{{{\rm{H}}_2}{\rm{O}}}^{liq}\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}
```

Because the system is not saturated with either pure O<sub>2</sub> gas, pure H<sub>2</sub>O fluid, or a mixed H<sub>2</sub>O-O<sub>2</sub> fluid, mass balance considerations demand that $n_{{{\rm{H}}_2}{\rm{O}}}^{liq} = n_{{{\rm{H}}_2}{\rm{O}}}^{sys}$ and $\frac{1}{2}n_{{{\rm{O}}_2}}^{sys} = n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq} + n_{{\rm{Si}}{{\rm{O}}_2}}^{liq} - n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}$. The equality constraints corresponding to this scenario can be expressed as:

```{math}
:label: eq47
\left[ {\begin{array}{*{20}{c}}
{{b_{{\rm{Si}}{{\rm{O}}_{\rm{2}}}}}}\\
{{b_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_{\rm{3}}}}}}\\
{{b_{{\rm{FeO}}}}}\\
{{b_{{{\rm{H}}_{\rm{2}}}{\rm{O}}}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
1&0&1&0&0&0\\
0&1&0&0&2&0\\
0&0&2&0&{ - 4}&0\\
0&0&0&1&0&1\\
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{F}}{{\rm{e}}_2}{\rm{Si}}{{\rm{O}}_4}}^{liq}}\\
{n_{{{\rm{H}}_2}{\rm{O}}}^{liq}}\\
{n_{{{\rm{O}}_2}}^{sys}}\\
{n_{{{\rm{H}}_2}{\rm{O}}}^{sys}}
\end{array}} \right]
```

where the last two rows account for the additional mass balance constraints. The fixed constraints may be decomposed using SVD as in Equation {eq}`eq19`

```{math}
:label: eq48
{\bf{C}}_f^T = \left[ {\begin{array}{*{20}{c}}
0&2&{ - 4}&0\\
0&0&0&1
\end{array}} \right] = {{\bf{U}}_f}{{\bf{S}}_f}{\bf{V}}_f^T = \left[ {\begin{array}{*{20}{c}}
1&0\\
0&1
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{2\sqrt 5 }&0&0&0\\
0&1&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
0&{\frac{1}{{\sqrt 5 }}}&{ - \frac{2}{{\sqrt 5 }}}&0\\
0&0&0&1\\
0&{\frac{2}{{\sqrt 5 }}}&{\frac{1}{{\sqrt 5 }}}&0\\
1&0&0&0
\end{array}} \right]
```

yielding

```{math}
:label: eq49
V_{\left. f \right|f}^T = \left[ {\begin{array}{*{20}{c}}
0&{\frac{2}{{\sqrt 5 }}}&{\frac{1}{{\sqrt 5 }}}&0\\
1&0&0&0
\end{array}} \right]
```

Multiplication of the constraint equation by this null-space projection operator gives

```{math}
:label: eq50
\left[ {\begin{array}{*{20}{c}}
0&{\frac{2}{{\sqrt 5 }}}&{\frac{1}{{\sqrt 5 }}}&0\\
1&0&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{{b_{{\rm{Si}}{{\rm{O}}_{\rm{2}}}}}}\\
{{b_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_{\rm{3}}}}}}\\
{{b_{{\rm{FeO}}}}}\\
{{b_{{{\rm{H}}_{\rm{2}}}{\rm{O}}}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
0&{\frac{2}{{\sqrt 5 }}}&{\frac{1}{{\sqrt 5 }}}&0\\
1&0&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
1&0&1&0&0&0\\
0&1&0&0&2&0\\
0&0&2&0&{ - 4}&0\\
0&0&0&1&0&1
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{F}}{{\rm{e}}_2}{\rm{Si}}{{\rm{O}}_4}}^{liq}}\\
{n_{{{\rm{H}}_2}{\rm{O}}}^{liq}}\\
{n_{{{\rm{O}}_2}}^{sys}}\\
{n_{{{\rm{H}}_2}{\rm{O}}}^{sys}}
\end{array}} \right]
```

and finally the projected bulk composition constraint matrix that we seek:

```{math}
:label: eq51
\left[ {\begin{array}{*{20}{c}}
{\frac{2}{{\sqrt 5 }}{b_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_{\rm{3}}}}} + \frac{1}{{\sqrt 5 }}{b_{{\rm{FeO}}}}}\\
{{b_{{\rm{Si}}{{\rm{O}}_{\rm{2}}}}}}
\end{array}} \right] = \left[ {\begin{array}{*{20}{c}}
0&{\frac{2}{{\sqrt 5 }}}&{\frac{2}{{\sqrt 5 }}}&0&0&0\\
1&0&1&0&0&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{F}}{{\rm{e}}_2}{\rm{Si}}{{\rm{O}}_4}}^{liq}}\\
{n_{{{\rm{H}}_2}{\rm{O}}}^{liq}}\\
{n_{{{\rm{O}}_2}}^{sys}}\\
{n_{{{\rm{H}}_2}{\rm{O}}}^{sys}}
\end{array}} \right]
```

The complete constraint vector is constructed by stacking this projection along with the fixed external constraints, e.g.Equation {eq}`eq24`

```{math}
:label: eq52
\begin{array}{*{20}{c}}
{\left[ {\begin{array}{*{20}{c}}
0&{\frac{2}{{\sqrt 5 }}}&{\frac{2}{{\sqrt 5 }}}&0\\
1&0&1&0
\end{array}} \right]\left[ {\begin{array}{*{20}{c}}
{n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq}}\\
{n_{{\rm{F}}{{\rm{e}}_2}{\rm{Si}}{{\rm{O}}_4}}^{liq}}\\
{n_{{{\rm{H}}_2}{\rm{O}}}^{liq}}
\end{array}} \right] - \left[ {\begin{array}{*{20}{c}}
{\frac{2}{{\sqrt 5 }}{b_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_{\rm{3}}}}} + \frac{1}{{\sqrt 5 }}{b_{{\rm{FeO}}}}}\\
{{b_{{\rm{Si}}{{\rm{O}}_{\rm{2}}}}}}
\end{array}} \right] = {\bf{0}}}\\
{\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq} - \mu _{{{\rm{H}}_2}{\rm{O}}}^{sys} = 0}\\
{2\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + 2\mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_3}}^{liq} - 2\mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq} - \mu _{{{\rm{O}}_2}}^{sys} = 0}
\end{array}
```

The last two rows of constraints reflect the fact that there is only one phase in the system, liquid. From Equation \ref{eq:52}, the $\bf{A}$ matrix may be derived

```{math}
:label: eq53
{\bf{A}} = \left[ {\begin{array}{*{20}{c}}
0&{\frac{2}{{\sqrt 5 }}}&{\frac{2}{{\sqrt 5 }}}&0\\
1&0&1&0\\
{ - \frac{{\partial \mu _{{{\rm{H}}_{\rm{2}}}{\rm{O}}}^{liq}}}{{\partial n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}}&{ - \frac{{\partial \mu _{{{\rm{H}}_{\rm{2}}}{\rm{O}}}^{liq}}}{{\partial n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_3}}^{liq}}}}&{ - \frac{{\partial \mu _{{{\rm{H}}_{\rm{2}}}{\rm{O}}}^{liq}}}{{\partial n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}}}&{ - \frac{{\partial \mu _{{{\rm{H}}_{\rm{2}}}{\rm{O}}}^{liq}}}{{\partial n_{{{\rm{H}}_{\rm{2}}}{\rm{O}}}^{liq}}}}\\
{ - \frac{{\partial \mu _{{{\rm{O}}_{\rm{2}}}}^{liq}}}{{\partial n_{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}}&{ - \frac{{\partial \mu _{{{\rm{O}}_{\rm{2}}}}^{liq}}}{{\partial n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_3}}^{liq}}}}&{ - \frac{{\partial \mu _{{{\rm{O}}_{\rm{2}}}}^{liq}}}{{\partial n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}}}&{ - \frac{{\partial \mu _{{{\rm{O}}_{\rm{2}}}}^{liq}}}{{\partial n_{{{\rm{H}}_{\rm{2}}}{\rm{O}}}^{liq}}}}
\end{array}} \right]
```

Note that the entries for the $\bf{A}$ matrix in the fourth row may be computed using the definition of the liquid chemical potential of oxygen:

```{math}
:label: eq54
\frac{{\partial \mu _{{{\rm{O}}_2}}^{liq}}}{{\partial n_i^{liq}}} = 2\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial n_i^{liq}}} + 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_3}}^{liq}}}{{\partial n_i^{liq}}} - 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}}{{\partial n_i^{liq}}}
```

The gradient of the Khorzhinskii potential is computed from Equation {eq}`eq46` as

```{math}
:label: eq55
{\bf{g}} = \frac{{\partial L}}{{\partial {{\bf{n}}_\phi }}} = \left[ {\begin{array}{*{20}{c}}
{\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{\mu _{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq}}\\
{\mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}\\
{\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}
\end{array}} \right] - \left[ {\begin{array}{*{20}{c}}
{\frac{{\partial n_{{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}}}\\
{\frac{{\partial n_{{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial n_{{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}}}}}\\
{\frac{{\partial n_{{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{{\rm{H}}_2}{\rm{O}}}}}}}
\end{array}} \right]\mu _{{{\rm{O}}_2}}^{liq} - n_{{{\rm{O}}_2}}^{liq}\left[ {\begin{array}{*{20}{c}}
{\frac{{\partial \mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}}}\\
{\frac{{\partial \mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial \mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}}}}}\\
{\frac{{\partial \mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{{\rm{H}}_2}{\rm{O}}}}}}}
\end{array}} \right] - \left[ {\begin{array}{*{20}{c}}
0\\
0\\
0\\
1
\end{array}} \right]\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq} - n_{{{\rm{H}}_2}{\rm{O}}}^{liq}\left[ {\begin{array}{*{20}{c}}
{\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}}}\\
{\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}}}}}\\
{\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_{{{\rm{H}}_2}{\rm{O}}}}}}}
\end{array}} \right]
```

```{math}
{\bf{g}} = \frac{{\partial L}}{{\partial {{\bf{n}}_\phi }}} = \left[ {\begin{array}{*{20}{c}}
{\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}\\
{\mu _{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq}}\\
{\mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}\\
{\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}
\end{array}} \right] + \left[ {\begin{array}{*{20}{c}}
{ - 4\mu _{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq} - 4\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + 4\mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}\\
{ - 4\mu _{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq} - 4\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} + 4\mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}\\
{4\mu _{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq} + 4\mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq} - 4\mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}\\
{ - \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}
\end{array}} \right] - n_{{{\rm{O}}_2}}^{liq}\left[ {\begin{array}{*{20}{c}}
{2\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}} + 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}} - 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}}}\\
{2\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}}}} + 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}}}} - 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}}}}}\\
{2\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}}}} + 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}}}} - 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}}}}}\\
{2\frac{{\partial \mu _{{\rm{Si}}{{\rm{O}}_2}}^{liq}}}{{\partial {n_{{{\rm{H}}_2}{\rm{O}}}}}} + 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{{\rm{O}}_3}}^{liq}}}{{\partial {n_{{{\rm{H}}_2}{\rm{O}}}}}} - 2\frac{{\partial \mu _{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}^{liq}}}{{\partial {n_{{{\rm{H}}_2}{\rm{O}}}}}}}
\end{array}} \right] - n_{{{\rm{H}}_2}{\rm{O}}}^{liq}\left[ {\begin{array}{*{20}{c}}
{\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_{{\rm{Si}}{{\rm{O}}_2}}}}}}\\
{\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}}}}}\\
{\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_{{\rm{F}}{{\rm{e}}_{\rm{2}}}{\rm{Si}}{{\rm{O}}_4}}}}}}\\
{\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_{{{\rm{H}}_2}{\rm{O}}}}}}}
\end{array}} \right]
```

```{math}
{g_i} = \frac{{\partial L}}{{\partial {n_i}}} = \mu _i^{liq} - \frac{{\partial n_{{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}}}\mu _{{{\rm{O}}_2}}^{liq} - n_{{{\rm{O}}_2}}^{liq}\frac{{\partial \mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}}} - \frac{{\partial n_{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_i}}}\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq} - n_{{{\rm{H}}_2}{\rm{O}}}^{liq}\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_i}}}
```

and the Hessian elements are given by

```{math}
{H_{i,j}} = \frac{{{\partial ^2}L}}{{\partial {n_i}\partial {n_j}}} = \frac{{\partial \mu _i^{liq}}}{{\partial {n_j}}} - \frac{{\partial n_{{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}}}\frac{{\partial \mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_j}}} - \frac{{\partial n_{{{\rm{O}}_2}}^{liq}}}{{\partial {n_j}}}\frac{{\partial \mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}}} - \frac{{\partial n_{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_i}}}\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_j}}} - \frac{{\partial n_{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_j}}}\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_i}}} - n_{{{\rm{O}}_2}}^{liq}\frac{{{\partial ^2}\mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}\partial {n_j}}} - n_{{{\rm{H}}_2}{\rm{O}}}^{liq}\frac{{{\partial ^2}\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_i}\partial {n_j}}}
```
and the Lagrangian is given by:

```{math}
:label: eq56
\Lambda  = L - {\lambda _{{\rm{F}}{{\rm{e}}_{Tot}}}}\left( {\frac{2}{{\sqrt 5 }}n_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{liq} + \frac{2}{{\sqrt 5 }}n_{{\rm{F}}{{\rm{e}}_2}{\rm{Si}}{{\rm{O}}_4}}^{liq} - \frac{2}{{\sqrt 5 }}{b_{{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}} - \frac{1}{{\sqrt 5 }}n_{{\rm{FeO}}}^{liq}} \right) - {\lambda _{{\rm{Si}}{{\rm{O}}_2}}}\left( {n_{{\rm{Si}}{{\rm{O}}_2}}^{liq} - {b_{{\rm{Si}}{{\rm{O}}_2}}}} \right) - {\lambda _{{{\rm{H}}_{\rm{2}}}{\rm{O}},\mu }}\left( {\mu _{{{\rm{H}}_{\rm{2}}}{\rm{O}}}^{liq} - \mu _{{{\rm{H}}_{\rm{2}}}{\rm{O}}}^{sys}} \right) - {\lambda _{{{\rm{O}}_2},\mu }}\left( {\mu _{{{\rm{O}}_2}}^{liq} - \mu _{{{\rm{O}}_2}}^{sys}} \right)
```

from which

```{math}
:label: eq57
\frac{{\partial \Lambda }}{{\partial {n_i}}} = \frac{{\partial L}}{{\partial {n_i}}} - {\lambda _{{\rm{F}}{{\rm{e}}_{Tot}}}}\left( {\frac{2}{{\sqrt 5 }}\delta _{i,{\rm{F}}{{\rm{e}}_2}{{\rm{O}}_3}}^{} + \frac{2}{{\sqrt 5 }}\delta _{i,{\rm{F}}{{\rm{e}}_2}{\rm{Si}}{{\rm{O}}_4}}^{}} \right) - {\lambda _{{\rm{Si}}{{\rm{O}}_2}}}\delta _{i,{\rm{Si}}{{\rm{O}}_2}}^{} - {\lambda _{{{\rm{H}}_{\rm{2}}}{\rm{O}},\mu }}\frac{{\partial \mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_i}}} - {\lambda _{{{\rm{O}}_2},\mu }}\frac{{\partial \mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}}}
```

and

```{math}
:label: eq58
\frac{{{\partial ^2}\Lambda }}{{\partial {n_i}\partial {n_j}}} = \frac{{{\partial ^2}L}}{{\partial {n_i}\partial {n_j}}} - {\lambda _{{{\rm{H}}_{\rm{2}}}{\rm{O}},\mu }}\frac{{{\partial ^2}\mu _{{{\rm{H}}_2}{\rm{O}}}^{liq}}}{{\partial {n_i}\partial {n_j}}} - {\lambda _{{{\rm{O}}_2},\mu }}\frac{{{\partial ^2}\mu _{{{\rm{O}}_2}}^{liq}}}{{\partial {n_i}\partial {n_j}}}
```
